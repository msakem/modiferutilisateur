﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Text;

using System.Windows.Forms;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.IE;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace UnitTestProject1
{
    [TestFixture]
    public class UntitledTestCase
    {
        private InternetExplorerDriver driver;
        /*private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;*/

        [SetUp]
        public void SetupTest()
        {


            var options = new InternetExplorerOptions()
            {
              // Proxy = GetProxy("acc.es2h.pp.s2h.corp"),
                IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                IgnoreZoomLevel = true,
                //EnableNativeEvents = false,
                UnhandledPromptBehavior = UnhandledPromptBehavior.Accept,
                //EnablePersistentHover = true
                UsePerProcessProxy=true,
            };

            driver = new InternetExplorerDriver();
            driver.Manage().Window.Maximize();

            //baseURL = "https://www.katalon.com/";
            //verificationErrors = new StringBuilder();
            //verificationErrors.Append("test");
        }


        public Proxy GetProxy(string host)
        {
            
            var proxy = new Proxy { HttpProxy = "inetproxy:83", SslProxy = "inetproxy:83", Kind = ProxyKind.Manual, IsAutoDetect = false };
            var adressByPass = "127.0.0.1;*.s2h.corp;*.test.s2hgroup.com;*.acshgroup.corp;*.lcf.fin.fr;10.*;siac24.siaci.fr;magica.fr;81.255.123.*;81.255.131.*;90.80.43.52;172.16.*;172.17*;213.11.58.188;citrix-assu.groupe-vinci.net;autodiscover;owliancegedmaarch.s2h.corp;mobiledoctors;psm-recette.neuros;psm.neuros;srvbiblos.ocirp.fr;*.diaboloacd.com;*.engage.diabolocom.com;*.dabcnet.net;mail-1.diabolocom.com;mail-2.diabolocom.com;pprod-s3t.emc.com;pprod-ws.s3t.emc.com;s3t.emc.com;ws.s3t.emc.com;sso.engage.diabolocom.com;168.183.167.31;test;160.92.59.85;160.92.42.19;web-prd40.cbw.as8677.net;web-eac40.cbw.as8677.net;*partnersolutions.ca;corp.s2hgroup.com";
            var tab = adressByPass.Split(';');



            for (var i = 0; i < tab.Length; i++) // Iterate through each exception
            {
                Regex hostRegex = new Regex(tab[i].Replace("*", @"(.)*"));

                var t = hostRegex.Match(host);

                if (t.Success) // Test regex query against hostname
                {
                    return null; // Bypass the proxy
                }
            }

            return proxy;
        }

        public bool Authenticate()
        {

            driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/login.html");
            //driver.FindElement(By.Id("moreInfoContainer")).Click()

            AcceptSSlCertficatIE();

            driver.FindElement(By.Id("_username")).Click();
            driver.FindElement(By.Id("_username")).SendKeys("Houda.MSAKEM@s2hgroup.com");
            driver.FindElement(By.Id("_password")).Click();
            driver.FindElement(By.Id("_password")).SendKeys("Bonbon000@");
            driver.FindElement(By.Id("authenticate")).Click();
            return true;
        }
        [Test]
        public void BSI_EX000200()
        {

            if (Authenticate())
            {

                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(2) > a:nth-child(1)")).Click();
                driver.FindElement(By.CssSelector("div.datagrid_row:nth-child(12) > div:nth-child(2) > a:nth-child(1)")).Click();
                driver.FindElement(By.Id("acc_administrator_firstName")).Click();
                driver.FindElement(By.Id("acc_administrator_firstName")).Clear();
                driver.FindElement(By.Id("acc_administrator_firstName")).SendKeys("DODOHFF");
                driver.FindElement(By.Id("acc_administrator_lastName")).Click();
                driver.FindElement(By.Id("acc_administrator_lastName")).Clear();
                driver.FindElement(By.Id("acc_administrator_lastName")).SendKeys("DODIFF");
                driver.FindElement(By.Id("acc_administrator_email")).Click();
                driver.FindElement(By.Id("acc_administrator_email")).Clear();
                driver.FindElement(By.Id("acc_administrator_email")).SendKeys("issam.lemlihH@gmail.com");
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Administrateur'])[1]/following::button[1]")).Click();
                Thread.Sleep(2000);
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(2) > a:nth-child(1)")).Click();
            }

        }


        public bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException e)
            {
                return false;
            }
        }

        public void AcceptSSlCertficatIE()
        {
            driver.Navigate().GoToUrl("javascript:document.getElementById('overridelink').click()");
        }


        [TearDown]
        public void TeardownTest()
        {
            driver.Quit();
        }

    }
}
